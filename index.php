<?php


require('Frog.php');
require('Ape.php');

$sheep = new Animal("Shaun");
echo "Name : " . $sheep->name . "<br>";
echo "legs : " . $sheep->legs . "<br>";
echo "cold_blooded : " . $sheep->cold_blooded . "<br><br>";

$kodok = new Frog("Buduk");
echo "Name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "cold_blooded : " . $kodok->cold_blooded . "<br>";
$kodok->jump();
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "cold_blooded : " . $sungokong->cold_blooded . "<br>";
$sungokong->yell();

?>